//
//  SWAppDelegate.h
//  BeaconFrito
//
//  Created by Pablo Formoso Estrada on 13/12/13.
//  Copyright (c) 2013 Pablo Formoso Estrada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
