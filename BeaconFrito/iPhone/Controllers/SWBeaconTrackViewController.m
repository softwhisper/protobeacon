//
//  SWBeaconTrackViewController.m
//  BeaconFrito
//
//  Created by Pablo Formoso Estrada on 13/12/13.
//  Copyright (c) 2013 Pablo Formoso Estrada. All rights reserved.
//

#import "SWBeaconTrackViewController.h"

@interface SWBeaconTrackViewController ()

@property (nonatomic, strong) NSUUID *swUUID;
@property (nonatomic, strong) NSString *swID;
@property (nonatomic, strong) CLLocationManager *locManager;


@end

@implementation SWBeaconTrackViewController

- (id)init {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    self = [super init];
    
    if (self) {

    }
    
    return self;
}

- (void)viewDidLoad {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    _swUUID = [[NSUUID alloc] initWithUUIDString:@"E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"];
    _swID = @"0";
    _locManager = [[CLLocationManager alloc] init];
    _locManager.delegate = self;
    
    [super viewDidLoad];
    [self startMonitoringForBeacon];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)startMonitoringForBeacon {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    CLBeaconRegion *region = [[CLBeaconRegion alloc] initWithProximityUUID:_swUUID
                                                                identifier:_swID];
    region.notifyEntryStateOnDisplay = YES;
    region.notifyOnEntry = YES;
    region.notifyOnExit = YES;
    
    [_locManager startMonitoringForRegion:region];
    [_locManager startRangingBeaconsInRegion:region];
}

#pragma mark - CoreLocationManager Delegate
- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
#ifndef NDEBUG
    NSLog(@"%s (line:%d) nDentro de la region del Beacon", __PRETTY_FUNCTION__, __LINE__);
#endif
    
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    if ([region.identifier isEqualToString:_swID]) {
        [self.view setBackgroundColor:[UIColor blackColor]];
    }
}

- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray *)beacons
               inRegion:(CLBeaconRegion *)region {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    if ([beacons count] > 0) {
        CLBeacon *nearest = [beacons firstObject];
        
        switch (nearest.proximity) {
            case CLProximityUnknown:
                [self.view setBackgroundColor:[UIColor grayColor]];
                break;
            case CLProximityFar:
                [self.view setBackgroundColor:[UIColor yellowColor]];
                break;
            case CLProximityNear:
                [self.view setBackgroundColor:[UIColor orangeColor]];
                break;
            case CLProximityImmediate:
                [self.view setBackgroundColor:[UIColor redColor]];
                break;
            default:
                [self.view setBackgroundColor:[UIColor blueColor]];
                break;
        }
        
    }
}

- (BOOL)textView:(UITextView *)tView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    CGRect textRect = [tView.layoutManager usedRectForTextContainer:tView.textContainer];
    CGFloat sizeAdjustment = tView.font.lineHeight * [UIScreen mainScreen].scale;
    
    if (textRect.size.height >= tView.frame.size.height - sizeAdjustment) {
        if ([text isEqualToString:@"\n"]) {
            [UIView animateWithDuration:0.2 animations:^{
                [tView setContentOffset:CGPointMake(tView.contentOffset.x, tView.contentOffset.y + sizeAdjustment)];
            }];
        }
    }
    
    return YES;
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
    [textView scrollRangeToVisible:textView.selectedRange];
}

@end
