//
//  SWBeaconTrackViewController.h
//  BeaconFrito
//
//  Created by Pablo Formoso Estrada on 13/12/13.
//  Copyright (c) 2013 Pablo Formoso Estrada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface SWBeaconTrackViewController : UIViewController <CLLocationManagerDelegate>

@end
