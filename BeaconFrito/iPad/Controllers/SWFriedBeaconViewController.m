//
//  SWFriedBeaconViewController.m
//  BeaconFrito
//
//  Created by Pablo Formoso Estrada on 13/12/13.
//  Copyright (c) 2013 Pablo Formoso Estrada. All rights reserved.
//

#import "SWFriedBeaconViewController.h"

@interface SWFriedBeaconViewController ()

@property (nonatomic, strong) CLLocationManager *locManager;
@property (nonatomic, strong) CBPeripheralManager *cbManager;

@property (weak, nonatomic) IBOutlet UITextView *logTextField;

@end

@implementation SWFriedBeaconViewController

- (void)viewDidLoad {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    [super viewDidLoad];
    
    NSUUID *proxUUID = [[NSUUID alloc] initWithUUIDString:kBeaconUUID];
    [self registerBeaconRegionWithUUID:proxUUID andIdentifier:kMySpace];
}

- (void)didReceiveMemoryWarning {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    [super didReceiveMemoryWarning];
}

#pragma mark - Beacon Methods
- (void)registerBeaconRegionWithUUID:(NSUUID *)proximityUUID andIdentifier:(NSString *)identifier {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    CLBeaconRegion *beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID
                                                                      identifier:identifier];
    
    // Register the Beacon
    [_locManager startMonitoringForRegion:beaconRegion];
    
    // Advertise Data
    NSDictionary *beaconPeripheralData = [beaconRegion peripheralDataWithMeasuredPower:nil];
    
    _cbManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil options:nil];
    [_cbManager startAdvertising:beaconPeripheralData];
}

#pragma mark - CoreLocation Delegate

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
}


- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray *)beacons
               inRegion:(CLBeaconRegion *)region {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
    if ([beacons count] > 0) {
        CLBeacon *nearest = [beacons firstObject];
        NSString *text = _logTextField.text;
        
        switch (nearest.proximity) {
            case CLProximityUnknown:
                [_logTextField setText:[text stringByAppendingString:@"\n Por ahí anda"]];
                break;
            case CLProximityFar:
                [_logTextField setText:[text stringByAppendingString:@"\n Está lejos"]];
                break;
            case CLProximityNear:
                [_logTextField setText:[text stringByAppendingString:@"\n Caliente caliente"]];
                break;
            case CLProximityImmediate:
                [_logTextField setText:[text stringByAppendingString:@"\n Te quemaste"]];
                break;
            default:
                break;
        }
    }
}

#pragma mark - CoreBluethoot Delegate
- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
#ifndef NDEBUG
    NSLog(@"%s (line:%d) %i", __PRETTY_FUNCTION__, __LINE__, (int)peripheral.state);
#endif
    
}

- (void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveReadRequest:(CBATTRequest *)request {
#ifndef NDEBUG
    NSLog(@"%s (line:%d)", __PRETTY_FUNCTION__, __LINE__);
#endif
    
}

-(void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error {
#ifndef NDEBUG
    NSLog(@"%s (line:%d) %@", __PRETTY_FUNCTION__, __LINE__, [error localizedDescription]);
#endif
    
}

@end
