//
//  SWFriedBeaconViewController.h
//  BeaconFrito
//
//  Created by Pablo Formoso Estrada on 13/12/13.
//  Copyright (c) 2013 Pablo Formoso Estrada. All rights reserved.
//

#define kBeaconUUID @"9032C3E1-6E6E-4824-A02F-1E7664D776E4"
#define kMySpace @"sw.pablo"

#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface SWFriedBeaconViewController : UIViewController <CLLocationManagerDelegate, CBPeripheralManagerDelegate>


@end
