//
//  main.m
//  BeaconFrito
//
//  Created by Pablo Formoso Estrada on 13/12/13.
//  Copyright (c) 2013 Pablo Formoso Estrada. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SWAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SWAppDelegate class]));
    }
}
